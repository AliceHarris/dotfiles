# Acknowledgements/software used

window manager: [dwm](https://dwm.suckless.org) (MIT/X license)

windowing system: [xorg](https://x.org)

dwm build: [dwm-flexipatch](https://github.com/bakkeby/dwm-flexipatch) (MIT/X license)

status bar: [dwmblocks](https://github.com/torrinfail/dwmblocks) (ISC license)

terminal emulator: [kitty](https://sw.kovidgoyal.net/kitty) (GPL 3.0)

launcher: [rofi](https://github.com/davatorium/rofi) (MIT/X license)

password manager: [keepassxc](https://github.com/keepassxreboot/keepassxc) (GPL 2.0 or 3.0)

compositor: [picom](https://github.com/yshui/picom) (Mozilla Public License 2.0 and MIT/X)

text editors: [vim](https://vim.org) (Vim license), ([doom](https://github.com/doomemacs/doomemacs))[emacs](https://gnu.org/software/emacs) (MIT/X and GPL 3.0 or later respectively)

screenshot tool: scrot

wallpaper tools: [feh](https://feh.finalrewind.org) (MIT/X license), xwinwrap

browser: [firefox](https://mozilla.org/firefox) (Mozilla Public License 2.0)

colour scheme: [everforest](https://github.com/sainnhe/everforest) (MIT/X license)
