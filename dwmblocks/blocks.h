//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", 	"echo ^c#222222^", 30, 0},
	{"", 
		"mem=\"$(free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g)\"; echo \" ^b#e68183^  ${mem} ^b#222222^\" ",
		30,		0},
	{"^b#a7c080^  /dev/sda1: ",
		"usage1=\"$(df --output=pcent /dev/sda1 | grep -o '[0-9]*')\"; \
		echo ${usage1}%",
		30, 		0},
	{"/dev/nvme0n1p4: ",
		"usage2=\"$(df --output=pcent /dev/nvme0n1p4 | grep -o '[0-9]*')\"; \
		echo \"${usage2}% ^b#222222^\"",
		30,             0},
	{"^b#d9bb80^  ",
		"usage=\"$(getcpupercentage.sh)\"; \
		echo \"${usage} ^b#222222^\"",
		5, 		0},
	{"^b#83b6af^ ﰝ ",
		"volume=\"$(pamixer --get-volume-human)\"; \
		echo \"${volume} ^b#222222^\"",
		1, 		10},
	{"^b#d39bb6^ 直 ",
		"network=\"$(iw dev wlp5s0 info | grep ssid | awk '{print $2}')\"; \
		bluetooth=\"$(rofi-bluetooth --status)\"; \
		echo \"${network} ${bluetooth} ^b#222222^\"",
		5, 		0},
	{"^b#87c095^  ", 
		"date=\"$(date '+%b %d (%a) %I:%M%p')\"; \
		echo ${date}",
		5,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
