echo "Warning! This script *will* write to file locations where you might already have config files. Press Ctrl-C to exit, or Enter to continue."
read throwaway

APT_CMD=$(which apt)
PACMAN_CMD=$(which pacman)

if [ ! -z $APT_CMD ]; then
	sudo apt update
	sudo apt install rofi libx11-dev libxft-dev libxinerama-dev emacs make gcc feh kitty picom
elif [ ! -z $PACMAN_CMD ]; then
	sudo pacman -Syu rofi libx11 Xorg-server emacs base-devel feh kitty picom
else
	echo "Warning! Supported package manager not found! You will not be able to install dependencies!"
fi

if [ ! -f ~/.emacs.d/bin/doom ]; then
	git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
	~/.emacs.d/bin/doom install
	git clone https://git.sr.ht/~theorytoe/everforest-theme ~/.emacs.d/everforest-theme
fi

sudo cp initdwm.sh /usr/local/bin/initdwm.sh
sudo cp setwallpaper.sh /usr/local/bin/setwallpaper.sh
sudo cp screenshot.sh /usr/local/bin/screenshot.sh
sudo cp getcpupercentage.sh /usr/local/bin/getcpupercentage.sh

cd ../dwm
sudo make clean install
cd ../dwmblocks
sudo make clean install
cd ../xwinwrap
make
sudo make install
cd ..
sudo cp -r xsessions/* /usr/share/xsessions

cd files
mkdir -p ~/.config/kitty
cp kitty.conf ~/.config/kitty
cp kitty-forest.conf ~/.config/kitty
mkdir -p ~/.config/picom
cp picom.conf ~/.config/picom/picom.conf
mkdir -p ~/.vim/autoload/
mkdir -p ~/.config/nvim/autoload/
mkdir -p ~/.vim/colors/
mkdir -p ~/.config/nvim/colors/
mkdir -p ~/.vim/doc/
mkdir -p ~/.config/nvim/doc/
cp vimrc ~/.vimrc
mkdir -p ~/.vim/UltiSnips
cp snippets ~/.vim/UltiSnips
cp zshrc ~/.zshrc
cp everforest-autoload.vim ~/.vim/autoload/everforest.vim
cp everforest-autoload.vim ~/.config/nvim/autoload/everforest.vim
cp everforest-colors.vim ~/.vim/colors/everforest.vim
cp everforest-colors.vim ~/.config/nvim/colors/everforest.vim
cp everforest.txt ~/.vim/doc/
cp everforest.txt ~/.config/nvim/doc/
mkdir -p ~/.config/zsh
cp zsh-syntax-highlighting.zsh ~/.config/zsh
mkdir -p ~/.config/rofi
cp config.rasi ~/.config/rofi/config.rasi
mkdir -p ~/.local/share/rofi/themes
cp everforest.rasi ~/.local/share/rofi/themes/everforest.rasi
mkdir -p ~/.local/share/dwm
cp -a autostart.sh ~/.local/share/dwm/autostart.sh
cd ../emacs
ln -sf $(pwd)/config.el ~/.doom.d/config.el
ln -sf $(pwd)/init.el ~/.doom.d/init.el
ln -sf $(pwd)/packages.el ~/.doom.d/packages.el
~/.emacs.d/bin/doom upgrade
