#!/usr/bin/sh
image="$HOME/Pictures/Wallpapers/solarpunk/$(ls ~/Pictures/Wallpapers/solarpunk | shuf -n 1)";
killall xwinwrap
if [ "${image##*.}" = "gif" ];
	then xwinwrap -ov -g 1929x1080+0+0 -- mpv --fullscreen\
		--on-all-workspaces\
		--loop-file --no-audio --no-osc --no-osd-bar -wid %WID --quiet \
		$image &
elif [ "${image##*.}" = "mp4" ];
	then xwinwrap -ov -g 1929x1080+0+0 -- mpv --fullscreen\
		--on-all-workspaces\
		--loop-file --no-audio --no-osc --no-osd-bar -wid %WID --quiet \
		$image &
	else feh --bg-scale $image
fi
