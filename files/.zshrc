# The following lines were added by compinstall

# zstyle ':completion:*' completer _list _expand _complete _ignored
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' substitute q
zstyle ':completion:*' rehash true
zstyle :compinstall filename '/home/alice/.zshrc'
autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install

# Prompt
eval "$(starship init zsh)"

# Plugins
autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic

autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

# Path
PATH=$PATH:$HOME/.local/bin
PATH=$PATH:$HOME/.cargo/bin
PATH=$PATH:$HOME/Sources/flutter/bin

# Node
# source /usr/share/nvm/init-nvm.sh

# Vim
export EDITOR=`which vim`
alias vim="nvim"
# Kitty
alias icat="kitty +kitten icat"
#
# Syntax highlighting
source $HOME/.config/zsh/zsh-syntax-highlighting.zsh
