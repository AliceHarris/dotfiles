# Alice's kinda solarpunk dotfiles

This is my personal dwm rice, using dwmblocks as a status bar, alongside Kitty, Rofi, and others. It's built around the everforest theme. 

Acknowledgements can be found [here](acknowledgements.md)

It's a heavy work-in-progress, and still relies on certain things that are only true on my system (mounted disks in status bar, wi-fi device name, wallpaper directory, etc). This isn't expected to work out of the box, but should be more or less debuggable from the output of the initdwm.sh script. 

### Installation
``` sh
git clone https://gitlab.com/AliceHarris/dotfiles
cd dotfiles
sh scripts/install.sh
```

After starting X, the initdwm.sh script (now on the path) should be executed (for example, by your display manager or xinitrc). An xsession file is provided. 

This config as a whole is licensed under the GPL 3.0, or, at your option, any later version. Code snippets and dependencies used have their own licenses. 
